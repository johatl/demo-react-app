import React from 'react';
import { connect } from 'react-redux';
import { DefaultButton } from 'office-ui-fabric-react/lib/Button';
import { SAMPLE_ACTION } from '../actions';

const Home = ({ data, onClick }) => (
    <div>
        <DefaultButton primary onClick={onClick}>Fire action</DefaultButton>
        <pre>
            {JSON.stringify(data)}
        </pre>
    </div>
)

const mapStateToProps = (state) => {
    return {
        data: state.app
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        onClick: () => dispatch(SAMPLE_ACTION)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Home);
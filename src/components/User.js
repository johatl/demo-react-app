import React from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import {Persona, PersonaSize} from 'office-ui-fabric-react/lib/Persona';
import {getUserData} from '../actions';

class User extends React.Component {
    componentWillMount() {
        const {match:{params:{id}}} = this.props;
        this.props.getUserData(id);
    }
    render() {
        const {data} = this.props;
        return <Persona size={ PersonaSize.extraLarge } { ...data } />
    }
}

const mapStateToProps = (state) => {
    const {user} = state; 
    return {
        data: {
            primaryText: user.name,
            secondaryText: user.email,
            tertiaryText: user.phone,
            optionalText: user.website
        }
    }
}

const mapDispatchToProps = dispatch => bindActionCreators({getUserData},dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(User);
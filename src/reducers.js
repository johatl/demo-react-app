import {combineReducers} from 'redux';

export const appReducer = (state = {}, action) => {
    switch (action.type) {
      case 'SAMPLE_ACTION':
        const {foo = 0} = state;
        const newFoo = foo + 1;
        
        return {
            foo: newFoo
        };

      default:
        return state
    }
  }

  export const userReducer = (state = {}, action) => {
    switch (action.type) {
      case 'DATA_SUCCESS':
        const {payload} = action;
        return {...payload};
      default:
        return state
    }
  }
  
export default combineReducers({
    app: appReducer,
    user: userReducer,
})
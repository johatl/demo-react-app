export const SAMPLE_ACTION = {
    type: 'SAMPLE_ACTION'
};

export const getUserData = (id) => {
    return dispatch => fetch(`https://jsonplaceholder.typicode.com/users/${id}`)
    .then(res => res.json())
    .then(
        payload => dispatch({type: 'DATA_SUCCESS', payload}),
        error => dispatch({type: 'DATA_ERROR', error})
    );
}
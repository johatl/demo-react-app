import React, { Component } from 'react';
import {BrowserRouter as Router, Route} from 'react-router-dom';
import { createStore, applyMiddleware, compose } from 'redux'
import thunk from 'redux-thunk';
import {Provider} from 'react-redux';
import reducers from './reducers';

import logo from './logo.svg';
import './App.css';

import Home from './components/Home';
import User from './components/User';

const middlewares = [thunk];
const enhanchers = [applyMiddleware(...middlewares)];
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const store = createStore(reducers, {}, composeEnhancers(...enhanchers));

class App extends Component {
  constructor() {
    super();
    console.log('Component created');
  }
  render() {
    return (
      <Provider store={store}>
      <Router>
        <div className="App">
          <header className="App-header">
            <img src={logo} className="App-logo" alt="logo" />
            <h1 className="App-title">Welcome to React</h1>
          </header>
          <div className="App-intro">
            <Route exact path="/" component={Home} />
            <Route path="/user/:id" component={User} />
          </div>
        </div>
      </Router>
      </Provider>
    );
  }
}

export default App;
